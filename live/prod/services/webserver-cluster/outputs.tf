output "elb_dns_name" {
  value     = module.webserver_cluster.elb_dns_name
}
  variable "server_port" {
  description   = "the server port will use for HTTP requests"
  default       = 8080
}