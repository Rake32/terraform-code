data "aws_availability_zones" "all" {}

data "aws_ami" "example" {
  owners      = ["amazon"]
  most_recent = true

filter {
  name  = "name"
  values = ["amzn2-ami-hvm-2.0.20200520.1-x86_64-gp2"]
}

}
