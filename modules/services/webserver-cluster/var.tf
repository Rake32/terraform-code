data "aws_availability_zones" "all" {}

data "aws_ami" "example" {
  owners      = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.20200520.1-x86_64-gp2"]
  }

}

variable "cluster_name" {
  description = "The name to use for all the cluster resources"
}

variable "db_remote_state_bucket" {
  description = "The name of the S3 bucket for the database's remote state"
}

variable "db_remote_state_key" {
  description = "The path for the database's remote state in S3"
}

variable "instance_type" {
  description = "Type of instance"
}

variable "min_size" {
  description = "Min number of EC2"
}

variable "max_size" {
  description = "Max number of EC2"
}

variable "enable_autoscaling" {
  description = "var to enable AS"
}

