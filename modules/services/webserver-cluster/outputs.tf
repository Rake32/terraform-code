output "elb_dns_name" {
  value = aws_elb.example.dns_name
}
variable "server_port" {
  description = "the server port will use for HTTP requests"
  default     = 8080
}

output "asg_name" {
  value = aws_autoscaling_group.example.name
}